//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\Metier\\Mouvements.java

package Metier;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Mouvements")
public class Mouvements 
{
	@DatabaseField
   private Date dateCreation;
   @DatabaseField
   private double montant;
   @DatabaseField
   private String description;
   
   @DatabaseField
   private int codeMouvement;
   @DatabaseField
   private int codeCompte;
   
   /**
    * @roseuid 3D2461860370
    */
   public Mouvements(double mont, String desc, int codeMouv, int codeCompte) 
   {
   		Calendar cal = new GregorianCalendar();
   		this.dateCreation = cal.getTime();
    	this.montant = mont;
    	this.description = desc;
    	this.codeMouvement = codeMouv;
    	this.codeCompte = codeCompte;
   }
   
   public Mouvements(Date dateCrea, double mont, String desc,int codeMouv,int codeCompte)
   {
	   this.dateCreation = dateCrea;
	   	this.montant = mont;
	   	this.description = desc;
	   	this.codeMouvement = codeMouv;
	   	this.codeCompte = codeCompte;
   }
   
   public Mouvements()
   {
	   
   }
   
   /**
    * @return Void
    * @roseuid 3D24608203FC
    */
   public Void afficher() 
   {
	   return null;
   }
   
   public Date getDateCreation() {
	return dateCreation;
	}
	
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	public int getCodeMouvement() {
		return codeMouvement;
	}
	
	public void setCodeMouvement(int codeMouvement) {
		this.codeMouvement = codeMouvement;
	}
	
	public int getCodeCompte() {
		return codeCompte;
	}
	
	public void setCodeCompte(int codeCompte) {
		this.codeCompte = codeCompte;
	}
	
	public void setMontant(double montant) {
		this.montant = montant;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate()
   {
	   Locale locale = Locale.getDefault();
	   DateFormat dateFormatFrancais = DateFormat.getDateInstance(DateFormat.FULL, locale);
	   DateFormat heureFormatFrancais = new SimpleDateFormat("HH'h'mm");
	   return dateFormatFrancais.format(this.dateCreation) + " � " + heureFormatFrancais.format(this.dateCreation);
   }
   
   public String getCodeMvt()
   {
   		return Integer.toString(codeMouvement);
   }
   
   public double getMontant()
   {
   		return montant;
   }
   
   public String getDescription()
   {
   		return description;
   }
}
