package Metier;

/**
 * Compte dans lequel on ne peut que d�poser de l'argent. Les interets sont
 * calcul� uniquement sur ce type de compte
 *
 * @author S219
 *
 */
public class CompteEpargne extends Comptes {
	public double getTauxInteret() {
		return this.tauxInteret;
	}

	private double tauxInteret = 0.2;

	/**
	 * @roseuid 3D246180033E
	 */
	public CompteEpargne(int iCodeCpt) {
		super(iCodeCpt);
	}

	/**
	 * @return Double
	 * @roseuid 3D24608203E9
	 */
	public double calculerInteret() {

		double dResultat = 0, dSolde;
		dSolde = this.getSolde();
		if (dSolde > 0) {
			dResultat = this.tauxInteret * dSolde;
		}
		return dResultat;
	}

	@Override
	public boolean debiter(double montant, String description, boolean isAdmin) {
		if ((this.getSolde() - montant) >= 0) {
			this.creerDebit(montant, description);
			return true;
		}
		else {
			return false;
		}
	}

	public void setTauxInteret(double newValue) {
		this.tauxInteret = newValue;
	}
}
