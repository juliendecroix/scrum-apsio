//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\Metier\\CompteDepot.java

package Metier;

/**
 * Compte dans lequel on peut d�poser et retirer de l'argent
 *
 * @author S219
 *
 */
public class CompteDepot extends Comptes {
	private double tauxAgios = 0.2;

	private double decouvertAutorise;
	private static double DECOUVERT_PAR_DEFAUT = 0.0;

	/**
	 * @roseuid 3D246183035C
	 */
	public CompteDepot(int iCodeCpt, Double decouvertAutorise) {
		super(iCodeCpt);
		if ((decouvertAutorise == null) || (decouvertAutorise < 0)) {
			this.decouvertAutorise = CompteDepot.DECOUVERT_PAR_DEFAUT;
		}
		else {
			this.decouvertAutorise = decouvertAutorise;
		}
	}

	/**
	 * @return Double
	 * @roseuid 3D24608203EF
	 */
	public double calculerAgios() {
		double dResultat = 0, dSolde;
		dSolde = this.getSolde();
		if (dSolde < 0) {
			dResultat = this.tauxAgios * dSolde * -1;
		}
		return dResultat;
	}

	/**
	 * D�bite seulement si le nouveau solde n'est pas inferieur au d�couvert
	 * autoris� OU si l'utilisateur est en mode admin.
	 *
	 * Renvoie true si l'op�ration a r�ussi, true sinon.
	 */
	@Override
	public boolean debiter(double montant, String description, boolean isAdmin) {
		if ((this.getSolde() - montant) >= -this.decouvertAutorise) {
			this.creerDebit(montant, description);
			return new Boolean(true);
		}
		else if (isAdmin) {
			this.creerDebit(montant, description + " (Debit exceptionel");
			return new Boolean(true);
		}
		else {
			return false;
		}
	}

	public double getDecouvertAutorise() {
		return this.decouvertAutorise;
	}

	public void setAgios(double d) {
		this.tauxAgios = d;
	}

}
