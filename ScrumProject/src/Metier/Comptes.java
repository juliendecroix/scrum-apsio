//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\Metier\\Comptes.java

package Metier;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

import com.j256.ormlite.dao.GenericRawResults;

public abstract class Comptes {
	private Date dateOuverture;
	protected double debit = 0.0;
	protected double credit = 0.0;
	private int codeCompte;
	public Vector<Mouvements> theMouvements = new Vector<Mouvements>();

	/**
	 * @roseuid 3D246189026C
	 */
	public Comptes(int iCodeCpt) {
		Calendar cal = new GregorianCalendar();
		this.dateOuverture = cal.getTime();
		this.codeCompte = iCodeCpt;
	}

	/**
	 * @return Double
	 * @roseuid 3D24608203D9
	 */
	public double getSolde() {
		double dResultat;
		dResultat = this.credit - this.debit;
		return dResultat;
	}

	/**
	 * @param montant
	 * @param description
	 * @return Boolean
	 * @roseuid 3D24608203DA
	 */
	public void crediter(double montant, String description) {
		this.credit = this.credit + montant;

		int iNbMvt = this.theMouvements.size();
		Mouvements mvt = MouvementORM.getInstance().CreateMouvement(montant, description, iNbMvt, this.codeCompte);
		this.theMouvements.add(mvt);
	}

	/**
	 * @return Integer
	 * @roseuid 3D24608203DD
	 */
	public int getCodeCompte() {
		return this.codeCompte;
	}

	/**
	 * @param compteCible
	 * @param montant
	 * @return Boolean
	 * @roseuid 3D24608203DE
	 */
	public void transferer(Comptes compteCible, double montant) {
		this.debit = this.debit + montant;
		compteCible.credit = compteCible.credit + montant;

	}

	/**
	 * @return Void
	 * @roseuid 3D24608203E1
	 */
	public String afficher() {

		return Double.toString(this.getSolde());
	}

	public String[][] getMouvements() {
		String[][] sMvt = new String[100][10];
		for (int i = 0; i < this.theMouvements.size(); i++) {
			Mouvements mvt = this.theMouvements.elementAt(i);
			sMvt[i][0] = mvt.getDate();
			sMvt[i][1] = mvt.getCodeMvt();
			double dMvt = mvt.getMontant();
			if (dMvt > 0) {
				sMvt[i][2] = Double.toString(dMvt);
				sMvt[i][3] = "-";
			}
			else {
				sMvt[i][3] = Double.toString(dMvt);
				sMvt[i][2] = "-";
			}
			sMvt[i][4] = mvt.getDescription();
		}
		return sMvt;
	}

	public void setMouvements(GenericRawResults<Mouvements> data) {
		for (Mouvements mvt : data) {
			this.theMouvements.add(mvt);

			// Credit
			if (mvt.getMontant() > 0) {
				this.credit += mvt.getMontant();
			}
			// Debit
			else {
				this.debit += mvt.getMontant();
			}
		}
	}

	protected void creerDebit(double montant, String description) {
		this.debit = this.debit + montant;

		int iNbMvt = this.theMouvements.size();
		Mouvements mvt = MouvementORM.getInstance().CreateMouvement(montant * -1, description, iNbMvt, this.codeCompte);
		this.theMouvements.add(mvt);
	}

	public abstract boolean debiter(double parseDouble, String text, boolean bAdministrateur);
}
