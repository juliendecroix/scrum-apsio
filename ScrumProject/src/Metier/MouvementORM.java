package Metier;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class MouvementORM {
	private static MouvementORM instance;

	private String databaseUrl = "jdbc:h2:./mvt";
	private ConnectionSource connectionSource;
	private Dao<Mouvements, String> mvtDao;

	public static MouvementORM getInstance() {
		if (MouvementORM.instance == null) {
			try {
				MouvementORM.instance = new MouvementORM();
			}
			catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}

		return MouvementORM.instance;
	}

	private MouvementORM() throws SQLException {
		this.connectionSource = new JdbcConnectionSource(this.databaseUrl);

		this.mvtDao = DaoManager.createDao(this.connectionSource, Mouvements.class);

		TableUtils.createTableIfNotExists(this.connectionSource, Mouvements.class);
	}

	public GenericRawResults<Mouvements> LoadMouvements(int codeCompte) {
		if (this.mvtDao != null) {
			try {

				GenericRawResults<Mouvements> results = this.mvtDao.queryRaw("SELECT * FROM `Mouvements` WHERE `codeCompte` = " + codeCompte,
						new RawRowMapper<Mouvements>() {
							@Override
							public Mouvements mapRow(String[] columnNames, String[] resultColumns) {

								Date date = new Date();

								try {
									date = DateFormat.getInstance().parse(resultColumns[0]);
								}
								catch (ParseException e) {
									date = new Date();
								}

								return new Mouvements(date, Double.parseDouble(resultColumns[1]), resultColumns[2], Integer
										.parseInt(resultColumns[3]), Integer.parseInt(resultColumns[4]));
							}
						});

				return results;

			}
			catch (Exception e) {

				return null;

			}

		}
		else {
			return null;
		}
	}

	public Mouvements CreateMouvement(double mont, String desc, int codeMouv, int codeCompte) {
		Mouvements mvt = new Mouvements(mont, desc, codeMouv, codeCompte);

		try {
			this.mvtDao.create(mvt);
			return mvt;
		}
		catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
