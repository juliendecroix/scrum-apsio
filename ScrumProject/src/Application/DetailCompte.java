//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\Application\\DetailCompte.java

package Application;

import java.awt.Dimension;
import java.util.Vector;

import javax.swing.JTable;

import Metier.CompteDepot;
import Metier.Comptes;

public class DetailCompte {
	public Vector theComptes;
	public JTable table;

	/**
	 * @roseuid 3D24647900AA
	 */
	public DetailCompte(ListeCompte listeCompte) {
		String sMvt[][] = new String[100][100];
		String nomColonnes[] = { "Compte", "Solde", "Decouvert autoris�", "Code mouvement3", "Date mouvement", "Cr�dit", "D�bit", "Description" };
		int ligneSurLaquelleOnEcrit = 0;

		for (int i = 0; i < listeCompte.size(); i++) {
			sMvt[ligneSurLaquelleOnEcrit][0] = Integer.toString(listeCompte.getCodeCompte(i));
			Comptes c = listeCompte.theComptes.elementAt(i);
			sMvt[ligneSurLaquelleOnEcrit][1] = Double.toString(c.getSolde());
			if (c instanceof CompteDepot) {
				CompteDepot compte = (CompteDepot) c;
				sMvt[ligneSurLaquelleOnEcrit][2] = Double.toString(compte.getDecouvertAutorise());
			}
			else {
				sMvt[ligneSurLaquelleOnEcrit][2] = "-";
			}

			String[][] sMouvement = new String[100][100];
			sMouvement = c.getMouvements();

			ligneSurLaquelleOnEcrit++;
			for (int mouvement = 0; mouvement < c.theMouvements.size(); mouvement++) {
				sMvt[ligneSurLaquelleOnEcrit][3] = sMouvement[mouvement][1];
				sMvt[ligneSurLaquelleOnEcrit][4] = sMouvement[mouvement][0];
				sMvt[ligneSurLaquelleOnEcrit][5] = sMouvement[mouvement][2];
				sMvt[ligneSurLaquelleOnEcrit][6] = sMouvement[mouvement][3];
				sMvt[ligneSurLaquelleOnEcrit][7] = sMouvement[mouvement][4];
				ligneSurLaquelleOnEcrit++;
			}
			ligneSurLaquelleOnEcrit++;
		}

		this.table = new JTable(sMvt, nomColonnes);
		this.table.setPreferredScrollableViewportSize(new Dimension(750, 400));
	}

	public JTable getTable() {
		return this.table;
	}

	public DetailCompte(Comptes c) {
		String sMvt[][] = new String[100][100];
		String nomColonnes[] = { "Compte", "Solde", "Decouvert autoris�", "Code mouvement", "Date mouvement", "Cr�dit", "D�bit", "Description" };
		int ligneSurLaquelleOnEcrit = 0;

		sMvt[ligneSurLaquelleOnEcrit][0] = Integer.toString(c.getCodeCompte());

		sMvt[ligneSurLaquelleOnEcrit][1] = Double.toString(c.getSolde());
		if (c instanceof CompteDepot) {
			CompteDepot compte = (CompteDepot) c;
			sMvt[ligneSurLaquelleOnEcrit][2] = Double.toString(compte.getDecouvertAutorise());
		}
		else {
			sMvt[ligneSurLaquelleOnEcrit][2] = "-";
		}

		String[][] sMouvement = new String[100][100];
		sMouvement = c.getMouvements();

		ligneSurLaquelleOnEcrit++;
		for (int mouvement = 0; mouvement < c.theMouvements.size(); mouvement++) {
			sMvt[ligneSurLaquelleOnEcrit][3] = sMouvement[mouvement][1];
			sMvt[ligneSurLaquelleOnEcrit][4] = sMouvement[mouvement][0];
			sMvt[ligneSurLaquelleOnEcrit][5] = sMouvement[mouvement][2];
			sMvt[ligneSurLaquelleOnEcrit][6] = sMouvement[mouvement][3];
			sMvt[ligneSurLaquelleOnEcrit][7] = sMouvement[mouvement][4];
			ligneSurLaquelleOnEcrit++;
		}
		ligneSurLaquelleOnEcrit++;

		this.table = new JTable(sMvt, nomColonnes);
		this.table.setPreferredScrollableViewportSize(new Dimension(750, 400));
	}

}
