//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\Application\\OuvrirCompte.java

package Application;

import Metier.*;

public class OuvrirCompte {
	public Comptes theComptes;
	double dDepot;
	int iCodeCompte;
	Double decouvertAutorise;

	/**
	 * @roseuid 3D246164006E
	 */
	public OuvrirCompte(ListeClient lCli, int iCodeCpt, int iCodeClient,
			double depotInitial, Double decouvertAutorise) {
		Clients cClients = null;
		for (int i = 0; i < lCli.theClients.size(); i++) {
			int iCodeCli = lCli.getCode(i);
			if (iCodeCli == iCodeClient) {
				cClients = (Clients) lCli.theClients.elementAt(i);
				break;
			}
		}

		iCodeCompte = iCodeCpt;
		dDepot = depotInitial;
		this.decouvertAutorise = decouvertAutorise;
		cClients.addCompteToClient(iCodeCompte);
	}

	public CompteEpargne createCompteEpargne() {
		CompteEpargne ce = new CompteEpargne(iCodeCompte);
		ce.crediter(dDepot, "D�p�t initial");
		return ce;
	}

	public CompteDepot createCompteDepot() {
		CompteDepot cd = new CompteDepot(iCodeCompte, decouvertAutorise);
		cd.crediter(dDepot, "D�p�t initial");
		return cd;
	}
}
