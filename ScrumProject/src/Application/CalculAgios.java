//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\Application\\CalculAgios.java

package Application;

import java.util.ArrayList;

import Metier.CompteDepot;

public class CalculAgios {
	public CompteDepot theCompteDepot;

	/**
	 * @roseuid 3D24617500BE
	 */
	public CalculAgios(ListeCompte lc, int iCodeCpt) {
		for (int i = 0; i < lc.size(); i++) {
			if (lc.getCodeCompte(i) == iCodeCpt) {
				this.theCompteDepot = (CompteDepot) lc.getCompte(i);
			}
		}
	}

	public static double DebiterAgio(ArrayList<CompteDepot> lc) {
		double totalAgio = 0;

		for (CompteDepot c : lc) {
			if (c.getSolde() < 0) {
				double agio = c.calculerAgios();

				c.debiter(agio, "Debit Agio", true);

				totalAgio += agio;
			}
		}

		return totalAgio;
	}

}
