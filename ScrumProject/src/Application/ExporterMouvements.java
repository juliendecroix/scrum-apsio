package Application;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import Metier.Comptes;
import Metier.Mouvements;

public class ExporterMouvements {

	public ExporterMouvements(Comptes compte, String path) {

		File file = new File(path);
		if (file.exists()) {
			file.delete();
		}

		FileWriter fw = null;
		BufferedWriter writer = null;
		try {
			file.createNewFile();
			fw = new FileWriter(file);
			writer = new BufferedWriter(fw);

			for (Mouvements mouvement : compte.theMouvements) {
				writer.append(mouvement.getCodeMvt() + " " + Double.toString(mouvement.getMontant()) + " " + mouvement.getDate() + " "
						+ mouvement.getDescription());
				writer.newLine();
				writer.flush();
			}
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if (fw != null) {
				try {
					fw.close();
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (writer != null) {
				try {
					writer.close();
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
