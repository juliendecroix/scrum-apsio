//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\Application\\DetailCompte.java

package Application;

import java.awt.Dimension;
import java.security.GeneralSecurityException;
import java.util.Vector;

import javax.swing.JTable;

import Metier.CompteDepot;
import Metier.CompteEpargne;
import Metier.Comptes;

public class DetailInterets {
	public Vector theComptes;
	public JTable table;

	/**
	 * @roseuid 3D24647900AA
	 */
	public DetailInterets(ListeCompte lstComptesEpargne) {
		// cases dans lesquelles sont rentr�es les �l�ments de la table
		// avec cases[numLigne][numColonne]
		String cases[][] = new String[100][100];
		String nomColonnes[] = { "Compte", "Solde", "Taux", "Interet" } ;
		int k = 0;

		for (int i = 0; i < lstComptesEpargne.size(); i++) {
			cases[k][0] = Integer.toString(lstComptesEpargne.getCodeCompte(i));
			CompteEpargne c = (CompteEpargne) lstComptesEpargne.theComptes.elementAt(i);
			cases[k][1] = Double.toString(c.getSolde());
//			if (c instanceof CompteDepot) {
//				CompteDepot compte = (CompteDepot) c;
//				cases[k][2] = Double.toString(compte.getDecouvertAutorise());
//			}
			// ajout du taux l'interet dans la case
			cases[k][2] = Double.toString(c.getTauxInteret());
			// ajout de la valeur de l'interet dans la case
			cases[k][3] = Double.toString(c.calculerInteret());
			k++;
		}

		this.table = new JTable(cases, nomColonnes);
		this.table.setPreferredScrollableViewportSize(new Dimension(750, 400));
	}

	public JTable getTable() {
		return this.table;
	}

}
