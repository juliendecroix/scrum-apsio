//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\Application\\ListeCompte.java

package Application;

import Metier.Comptes;
import Metier.MouvementORM;
import Metier.CompteEpargne;
import Metier.CompteDepot;
import Metier.Clients;
import java.util.Vector;

public class ListeCompte {
	public Vector<Comptes> theComptes;

	/**
	 * @roseuid 3D24647900AA
	 */
	public ListeCompte() {
		theComptes = new Vector<Comptes>();
		addCompteEpargne(0);
		addCompteEpargne(1);
		addCompteEpargne(2);
		addCompteDepot(3);
		addCompteDepot(4);
	}

	public void addCompteEpargne(int iCodeCpt) {
		CompteEpargne cpt = new CompteEpargne(iCodeCpt);
		
		cpt.setMouvements( MouvementORM.getInstance().LoadMouvements(iCodeCpt) );
		
		theComptes.add(cpt);
	}

	public void addCompteDepot(int iCodeCpt) {
		addCompteDepot(iCodeCpt, 0.);
	}
	
	public void addCompteDepot(int iCodeCpt, Double decouvertAutorise){
		CompteDepot cpt = new CompteDepot(iCodeCpt, decouvertAutorise);
		
		cpt.setMouvements( MouvementORM.getInstance().LoadMouvements(iCodeCpt) );
		
		theComptes.addElement(cpt);
	}

	public void addCompteEpargne(CompteEpargne Cpt) {
		theComptes.add(Cpt);
	}

	public void addCompteDepot(CompteDepot Cpt) {
		theComptes.add(Cpt);
	}

	public String afficher(int iIndice) {
		if (iIndice > -1 && iIndice < theComptes.size()) {
			Comptes c = (Comptes) theComptes.elementAt(iIndice);
			return Integer.toString(c.getCodeCompte());
		} else {
			System.out.println("Erreur d'indice : " + iIndice);
			return "";
		}
	}

	public int getCodeCompte(int iIndice) {
		if (iIndice > -1 && iIndice < theComptes.size()) {
			Comptes c = (Comptes) theComptes.elementAt(iIndice);
			return c.getCodeCompte();
		} else {
			System.out.println("Erreur d'indice : " + iIndice);
			return -1;
		}
	}

	public Comptes getCompte(int iCode) {
		Comptes c = null;
		for (int i = 0; i < size(); i++) {
			if (getCodeCompte(i) == iCode) {
				c = (Comptes) theComptes.elementAt(i);
			}
		}
		if (c == null) {
			System.out.println("Erreur de r�cup�ration du compte : " + iCode);
		}
		return c;
	}

	public CompteDepot getCompteDepot(int iCode) {
		CompteDepot c = null;
		for (int i = 0; i < size(); i++) {
			if (getCodeCompte(i) == iCode) {
				c = (CompteDepot) theComptes.elementAt(i);
			}
		}
		if (c == null) {
			System.out.println("Erreur de r�cup�ration du compte : " + iCode);
		}
		return c;
	}

	public boolean supprimerComptes(int iCode) {
		for (int i = 0; i < size(); i++) {
			if (getCodeCompte(i) == iCode) {
				theComptes.remove(i);
			}
		}
		return true;
	}

	public int size() {
		return theComptes.size();
	}

	public int mouvementSize(int iCpt) {
		int iSize = 0;
		for (int i = 0; i < theComptes.size(); i++) {
			Comptes c = (Comptes) theComptes.elementAt(i);
			if (c.getCodeCompte() == iCpt) {
				iSize = c.theMouvements.size();
			}
		}

		return iSize;
	}

	/**
	 * Cr�e un mouvement et appelle la fonction credit ou d�bit correspondant (en fonction du montant)
	 * @param iCodeCpt
	 * @param dMontant
	 * @param sDesc
	 * @param bAdmin
	 * @return
	 */
	public boolean addMouvement(int iCodeCpt, double dMontant, String sDesc,
			boolean bAdmin) {
		boolean bOK = true;

		for (int i = 0; i < theComptes.size(); i++) {
			Comptes c = (Comptes) theComptes.elementAt(i);
			if (c.getCodeCompte() == iCodeCpt) {
				
				// Debit
				if (dMontant < 0) {
					c.debiter(dMontant, sDesc, true);
				} 
				
				// Credit
				else {
					c.crediter(dMontant, sDesc);
				}
			}
		}
		return bOK;

	}

}
