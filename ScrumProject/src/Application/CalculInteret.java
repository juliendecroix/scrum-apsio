//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\Application\\CalculInteret.java

package Application;

import java.util.List;

import Metier.CompteEpargne;

public class CalculInteret {
	public CompteEpargne theCompteEpargne;

	public static double calculerInterets(List<CompteEpargne> comptesEpargne) {
		double totalValueAdded = 0;
		for (CompteEpargne compte : comptesEpargne) {
			double interet = compte.calculerInteret();
			compte.crediter(interet, "Calcul d'interet");
			totalValueAdded += interet;
		}
		return totalValueAdded;
	}
}
