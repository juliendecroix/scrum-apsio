//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\IHM\\DlgDebiter.java

package IHM;

import java.awt.Choice;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class DlgDebiter extends JFrame {
	DlgMain dlgMain;

	Choice cbCompte;
	JButton bDebiter;
	JButton bAnnuler;
	TextField tfMontant;
	TextField tfDesc;
	int iCptSel;
	AdaptateurBoutons unAdaptateurBoutons;

	public DlgDebiter(DlgMain dlg, int iIndex) {
		this.dlgMain = dlg;

		this.cbCompte = new Choice();
		this.ReloadListe();
		this.cbCompte.select(iIndex);
		this.iCptSel = Integer.parseInt(this.cbCompte.getItem(iIndex));

		Panel pCompte = new Panel();
		FlowLayout flCompte = new FlowLayout();
		pCompte.setLayout(flCompte);
		pCompte.add(new Label("Compte en cours :"));
		pCompte.add(this.cbCompte);

		Panel pBouton = new Panel();
		FlowLayout flBouton = new FlowLayout();
		pBouton.setLayout(flBouton);
		this.bDebiter = new JButton("D�biter");
		this.bAnnuler = new JButton("Annuler");
		pBouton.add(this.bDebiter);
		pBouton.add(this.bAnnuler);

		Panel pMontant = new Panel();
		FlowLayout flMontant = new FlowLayout();
		pMontant.setLayout(flMontant);
		pMontant.add(new Label("Montant :"));
		this.tfMontant = new TextField("", 20);
		pMontant.add(this.tfMontant);

		Panel pDescription = new Panel();
		FlowLayout flDescription = new FlowLayout();
		pDescription.setLayout(flDescription);
		pDescription.add(new Label("Description :"));
		this.tfDesc = new TextField("", 20);
		pDescription.add(this.tfDesc);

		Panel pInfo = new Panel();
		pInfo.add(pMontant);
		pInfo.add(pDescription);

		this.getContentPane().add("North", pCompte);
		this.getContentPane().add("Center", pInfo);
		this.getContentPane().add("South", pBouton);

		this.unAdaptateurBoutons = new AdaptateurBoutons();
		this.bDebiter.addActionListener(this.unAdaptateurBoutons);
		this.bAnnuler.addActionListener(this.unAdaptateurBoutons);

		this.addWindowListener(new AdapFenetre());

		this.setTitle("D�biter un compte");

		WindowUtils.centerWindow(this);

		this.pack();
		this.show();

		this.cbCompte.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				DlgDebiter.this.iCptSel = Integer.parseInt(DlgDebiter.this.cbCompte.getSelectedItem());
			}
		}

				);
	}

	public void ReloadListe() {
		if (this.cbCompte.getItemCount() > 0) {
			this.cbCompte.removeAll();
		}

		for (int i = 0; i < this.dlgMain.listeCompte.size(); i++) {
			String sCode = this.dlgMain.listeCompte.afficher(i);
			this.cbCompte.add(sCode);
		}
	}

	class AdaptateurBoutons implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == DlgDebiter.this.bDebiter) {
				boolean debited = DlgDebiter.this.dlgMain.listeCompte.getCompte(DlgDebiter.this.iCptSel).debiter(
						Double.parseDouble(DlgDebiter.this.tfMontant.getText()), DlgDebiter.this.tfDesc.getText(),
						DlgDebiter.this.dlgMain.bAdministrateur);
				if (debited) {
					JOptionPane.showMessageDialog(null, "Debit effectu�");
				}
				else {
					JOptionPane.showMessageDialog(null, "Le d�bit n'est pas autoris�.", "Erreur : Transaction annul�e", JOptionPane.ERROR_MESSAGE);
				}
				DlgDebiter.this.setVisible(false);
			}
			else if (e.getSource() == DlgDebiter.this.bAnnuler) {
				DlgDebiter.this.setVisible(false);
			}
		}
	}// fin de AdaptateurBoutons

	class AdapFenetre extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			DlgDebiter.this.setVisible(false);
		}
	}

}
