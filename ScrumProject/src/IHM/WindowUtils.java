package IHM;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class WindowUtils {
	public static void centerWindow(JFrame frame) {
		Dimension tailleEcran = Toolkit.getDefaultToolkit().getScreenSize();
		Point positionFenetre = new Point((tailleEcran.width / 2) - (frame.getWidth() / 2), (tailleEcran.height / 2) - (frame.getHeight() / 2));
		frame.setLocation(positionFenetre);
	}
}
