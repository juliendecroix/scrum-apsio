//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\IHM\\DlgDetailCompte.java

package IHM;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Application.ConsulterCompte;
import Application.DetailCompte;

public class DlgDetailCompte extends JFrame {
	DlgMain dlgMain;
	JButton bFermer;
	AdaptateurBoutons unAdaptateurBoutons;

	private boolean showAll;

	public ConsulterCompte theConsulterCompte;

	public DlgDetailCompte(DlgMain dlg) {
		this.dlgMain = dlg;

		DetailCompte dc = new DetailCompte(this.dlgMain.listeCompte);

		JTable table;
		JTable tableOne;
		JScrollPane scrollpane;

		table = dc.getTable();
		scrollpane = new JScrollPane(table);

		this.bFermer = new JButton("Fermer");

		this.getContentPane().add("Center", scrollpane);
		this.getContentPane().add("South", this.bFermer);

		this.addWindowListener(new AdapFenetre());
		this.unAdaptateurBoutons = new AdaptateurBoutons();
		this.bFermer.addActionListener(this.unAdaptateurBoutons);

		this.setTitle("D�tail des comptes");

		WindowUtils.centerWindow(this);

		this.showAll = true;

		this.pack();
		this.show();
	}

	public DlgDetailCompte(DlgMain dlg, int codeId) {

		this.dlgMain = dlg;

		DetailCompte dc = new DetailCompte(this.dlgMain.listeCompte.getCompte(codeId));

		JTable table;
		JTable tableOne;
		JScrollPane scrollpane;

		table = dc.getTable();
		scrollpane = new JScrollPane(table);

		this.bFermer = new JButton("Fermer");

		this.getContentPane().add("Center", scrollpane);
		this.getContentPane().add("South", this.bFermer);

		this.addWindowListener(new AdapFenetre());
		this.unAdaptateurBoutons = new AdaptateurBoutons();
		this.bFermer.addActionListener(this.unAdaptateurBoutons);

		this.setTitle("D�tail des comptes");

		Dimension tailleEcran = Toolkit.getDefaultToolkit().getScreenSize();
		Point positionFenetre = new Point((tailleEcran.width / 2) - (this.getWidth() / 2), (tailleEcran.height / 2) - (this.getHeight() / 2));
		this.setLocation(positionFenetre);

		this.showAll = true;

		this.pack();
		this.show();

		// dlgMain = dlg;
		//
		// DetailCompte dc = new DetailCompte(dlgMain.listeCompte);
		//
		// JLabel label;
		//
		// JScrollPane scrollpane;
		//
		// label = new JLabel();
		//
		// Comptes c = dlg.listeCompte.getCompte(codeId);
		//
		// String text = "<html>";
		//
		// if (c instanceof CompteDepot)
		// {
		// CompteDepot compte = (CompteDepot) c;
		//
		// text += "Code compte : " + c.getCodeCompte() + " | Type : Depot" +
		// " | Solde : "+ c.getSolde() +
		// " | D�couvert : "+compte.getDecouvertAutorise();
		// }
		// else
		// {
		// CompteEpargne compte = (CompteEpargne) c;
		//
		// text += "Code compte : " + c.getCodeCompte() + " | Type : Epargne" +
		// " | Solde : "+ c.getSolde() +
		// " | Inter�ts : "+compte.getTauxInteret();
		// }
		//
		//
		// String[][] sMouvement = new String[100][100];
		// sMouvement = c.getMouvements();
		//
		// for ( int i = 0 ; sMouvement[i][2] != null ; i++ ) {
		// ArrayList<String> tabTexte = new ArrayList<String>() ;
		// tabTexte.addAll(Arrays.asList(sMouvement[i][2], sMouvement[i][4],
		// sMouvement[i][5], sMouvement[i][6] , sMouvement[i][7]) );
		//
		// int j = 0 ;
		// while (j < tabTexte.size()) {
		// if (tabTexte.get(j) == null) {
		// tabTexte.remove(j) ;
		// } else {
		// j ++ ;
		// }
		// }
		// String ligneDeTexte = "" ;
		// for ( String element : tabTexte ) {
		// ligneDeTexte += " | " + element;
		// }
		// text += "<br>" + ligneDeTexte ;
		// }
		// text += "</html>" ;
		//
		// label.setText(text);
		//
		//
		// scrollpane = new JScrollPane(label);
		//
		// bFermer = new JButton("Fermer");
		//
		// getContentPane().add("Center",scrollpane);
		// getContentPane().add("South",bFermer);
		//
		// addWindowListener ((WindowListener)new AdapFenetre());
		// unAdaptateurBoutons = new AdaptateurBoutons();
		// bFermer.addActionListener(unAdaptateurBoutons);
		//
		// setTitle("D�tail du compte");
		//
		// showAll = true;
		//
		// pack();
		// show();
	}

	class AdaptateurBoutons implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == DlgDetailCompte.this.bFermer) {
				DlgDetailCompte.this.setVisible(false);
			}
		}
	}// fin de AdaptateurBoutons

	class AdapFenetre extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			DlgDetailCompte.this.setVisible(false);
		}
	}
}
