package IHM;

import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import Metier.CompteEpargne;

public class DlgModifierInteret extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = 3188693373243048415L;
	TextField tfMontant;
	JButton bModifier;
	AdaptateurBoutons adapteur;
	CompteEpargne compte;

	public DlgModifierInteret(CompteEpargne compteToChange) {
		Panel panel = new Panel();
		this.compte = compteToChange;
		this.adapteur = new AdaptateurBoutons();

		panel.add(new Label("Entrez la nouvelle valeur de l'interet (1 = 100%)"));

		this.tfMontant = new TextField("", 5);
		this.bModifier = new JButton("Valider");

		panel.add(this.tfMontant);
		panel.add(this.bModifier);

		this.add(panel);

		this.bModifier.addActionListener(this.adapteur);

		this.setTitle("Modifier Interet du compte " + compteToChange.getCodeCompte());

		WindowUtils.centerWindow(this);

		this.pack();
		this.show();
	}

	class AdaptateurBoutons implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == DlgModifierInteret.this.bModifier) {
				DlgModifierInteret.this.compte.setTauxInteret(Double.parseDouble(DlgModifierInteret.this.tfMontant.getText()));
				DlgModifierInteret.this.setVisible(false);
			}
		}
	}// fin de AdaptateurBoutons
}
