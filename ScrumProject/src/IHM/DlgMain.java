package IHM;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import Application.CalculAgios;
import Application.CalculInteret;
import Application.EditionAvertissements;
import Application.EditionReleves;
import Application.ListeClient;
import Application.ListeCompte;
import Metier.CompteDepot;
import Metier.CompteEpargne;
import Metier.Comptes;

public class DlgMain extends JFrame {
	public DlgListeCompte theDlgListeCompte;
	public DlgListeClient theDlgListeClient;
	public DlgCreateClient theDlgCreateClient;
	public DlgConnexionAdmin theDlgConnexionAdmin;
	public CalculAgios theCalculAgios;
	public CalculInteret theCalculInteret;
	public EditionReleves theEditionReleves;
	public EditionAvertissements theEditionAvertissements;

	JButton bCompte;
	JButton bClient;
	JButton bAdmin;
	JButton bInteret;
	JButton bAgios;
	JButton bQuitter;
	AdaptateurBoutons unAdaptateurBoutons;

	Vector vCompte = new Vector();
	ListeClient listeClient;
	ListeCompte listeCompte;
	boolean bAdministrateur;

	DlgMain dlgMain;

	public DlgMain(boolean isAdmin) {
		this.createWindow(isAdmin);

		// initialisations
		this.listeClient = new ListeClient();
		this.listeCompte = new ListeCompte();

		this.listeClient.addCompteToClient(0, 1);
		this.listeClient.addCompteToClient(2, 1);
		this.listeClient.addCompteToClient(1, 2);
		this.listeClient.addCompteToClient(3, 2);
		this.listeClient.addCompteToClient(4, 2);

		/*
		 * this.listeCompte.addMouvement(0, 100, "Cr�ation par d�faut", true);
		 * this.listeCompte.addMouvement(0, -20, "Cr�ation par d�faut", true);
		 * this.listeCompte.addMouvement(1, 10000, "Cr�ation par d�faut", true);
		 * this.listeCompte.addMouvement(2, 100, "Cr�ation par d�faut", true);
		 */

		this.dlgMain = this;

	}

	public void createWindow(boolean isAdmin) {
		// Cr�ation des controles
		this.bAdministrateur = isAdmin;
		this.getContentPane().removeAll();

		this.bCompte = new JButton("Comptes");
		this.bClient = new JButton("Clients");
		if (this.bAdministrateur) {
			this.bAdmin = new JButton("Passer en mode Employ�");
		}
		else {
			this.bAdmin = new JButton("Passer en mode Administrateur");
		}
		this.bAgios = new JButton("Calculer les Agios");
		this.bInteret = new JButton("Calculer les Int�r�ts");
		this.bQuitter = new JButton("Quitter l'application");

		// evenements sur controles
		this.unAdaptateurBoutons = new AdaptateurBoutons();
		this.bCompte.addActionListener(this.unAdaptateurBoutons);
		this.bClient.addActionListener(this.unAdaptateurBoutons);
		this.bAdmin.addActionListener(this.unAdaptateurBoutons);
		this.bAgios.addActionListener(this.unAdaptateurBoutons);
		this.bInteret.addActionListener(this.unAdaptateurBoutons);
		this.bQuitter.addActionListener(this.unAdaptateurBoutons);

		this.addWindowListener(new AdapFenetre());

		// ajout des controles

		this.getContentPane().setLayout(new GridLayout(2, 3));
		this.getContentPane().add(this.bAdmin);
		this.getContentPane().add(this.bCompte);
		if (!this.bAdministrateur) {
			this.getContentPane().add(this.bClient);
			this.getContentPane().add(this.bQuitter);
		}
		if (this.bAdministrateur) {
			this.getContentPane().add(this.bAgios);
			this.getContentPane().add(this.bInteret);
		}

		if (this.bAdministrateur) {
			this.setTitle("Gestion des comptes - Administrateur");
		}
		else {
			this.setTitle("Gestion des comptes - Employ�");
		}

		WindowUtils.centerWindow(this);

		this.pack();
		this.show();
	}

	class AdaptateurBoutons implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == DlgMain.this.bCompte) {
				DlgMain.this.theDlgListeCompte = new DlgListeCompte(DlgMain.this.dlgMain, -1);
			}
			else if (e.getSource() == DlgMain.this.bClient) {
				DlgMain.this.theDlgListeClient = new DlgListeClient(DlgMain.this.dlgMain);
			}
			else if (e.getSource() == DlgMain.this.bQuitter) {
				System.exit(0);
			}
			else if (e.getSource() == DlgMain.this.bAdmin) {
				// DlgMain.this.createWindow(!DlgMain.this.bAdministrateur);
				if (DlgMain.this.bAdministrateur) {
					DlgMain.this.createWindow(false);
				}
				else {
					DlgMain.this.theDlgConnexionAdmin = new DlgConnexionAdmin(DlgMain.this.dlgMain);
				}
			}

			else if (e.getSource() == DlgMain.this.bAgios) {
				double totalAgio = CalculAgios.DebiterAgio(DlgMain.this.getComptesDepot());

				JOptionPane.showMessageDialog(null, "Somme des agio d�bit�s : " + totalAgio);

			}
			else if (e.getSource() == DlgMain.this.bInteret) {
				double total = CalculInteret.calculerInterets(DlgMain.this.getComptesEpargne());
				JOptionPane.showMessageDialog(null, "Interets calcul�s et effectu�. Valeur total : " + total);
			}
		}

	}// fin de AdaptateurBoutons

	public List<CompteEpargne> getComptesEpargne() {
		List<CompteEpargne> comptes = new ArrayList<CompteEpargne>();
		for (Comptes compte : this.listeCompte.theComptes) {
			if (compte instanceof CompteEpargne) {
				comptes.add((CompteEpargne) compte);
			}
		}
		return comptes;
	}

	class AdapFenetre extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			System.exit(0);
		}
	}

	public ArrayList<CompteDepot> getComptesDepot() {
		ArrayList<CompteDepot> cpts = new ArrayList<>();

		for (Comptes c : this.listeCompte.theComptes) {
			if (c instanceof CompteDepot) {
				cpts.add((CompteDepot) c);
			}
		}

		return cpts;
	}

}// fin du main
