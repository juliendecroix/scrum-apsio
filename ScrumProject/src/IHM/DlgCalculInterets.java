package IHM;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Application.ConsulterCompte;
import Application.DetailCompte;

public class DlgCalculInterets extends JFrame {
	DlgMain dlgMain;
	JButton bFermer;
	AdaptateurBoutons unAdaptateurBoutons;

	public ConsulterCompte theConsulterCompte;

	public DlgCalculInterets(DlgMain dlg) {
		this.dlgMain = dlg;

		DetailCompte dc = new DetailCompte(this.dlgMain.listeCompte);
		JTable table = dc.getTable();
		JScrollPane scrollpane = new JScrollPane(table);

		this.bFermer = new JButton("Fermer");

		this.getContentPane().add("Center", scrollpane);
		this.getContentPane().add("South", this.bFermer);

		this.addWindowListener(new AdapFenetre());
		this.unAdaptateurBoutons = new AdaptateurBoutons();
		this.bFermer.addActionListener(this.unAdaptateurBoutons);

		this.setTitle("D�tail des comptes");

		WindowUtils.centerWindow(this);

		this.pack();
		this.show();
	}

	class AdaptateurBoutons implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == DlgCalculInterets.this.bFermer) {
				DlgCalculInterets.this.setVisible(false);
			}
		}
	}// fin de AdaptateurBoutons

	class AdapFenetre extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			DlgCalculInterets.this.setVisible(false);
		}
	}
}
