//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\IHM\\DlgListeCompte.java

package IHM;

import java.awt.FlowLayout;
import java.awt.List;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import Metier.CompteDepot;
import Metier.CompteEpargne;
import Metier.Comptes;

public class DlgListeCompte extends JFrame {
	DlgMain dlgMain;

	public DlgDetailCompte theDlgDetailCompte;
	public DlgCrediter theDlgCrediter;
	public DlgCreateCompte theDlgCreateCompte;
	public DlgDebiter theDlgDebiter;
	public DlgTransferer theDlgTransferer;

	AdaptateurBoutons unAdaptateurBoutons;
	JButton bCrediter;
	JButton bDebiter;
	JButton bNouveau;
	JButton bAllDetails;
	JButton bDetails;
	JButton bExport;
	JButton bSupprimer;
	JButton bFermer;
	JButton bTransferer;
	JButton bModifierInteret;
	JButton bModifierAgios;
	List listCompte;
	int iCodeClient;

	public DlgListeCompte(DlgMain dlg, int iCClient) {
		this.dlgMain = dlg;
		this.iCodeClient = iCClient;

		this.listCompte = new List(5, false);

		this.ReloadListe();

		// Ajout des boutons
		Panel pBouton = new Panel();
		FlowLayout flBouton = new FlowLayout();
		pBouton.setLayout(flBouton);
		this.bCrediter = new JButton("Cr�diter");
		if (dlg.bAdministrateur) {
			this.bDebiter = new JButton("D�bit Exceptionel");
		}
		else {
			this.bDebiter = new JButton("D�biter");
		}
		this.bModifierInteret = new JButton("Modifier interet");
		this.bModifierAgios = new JButton("Modifier agios");
		this.bTransferer = new JButton("Transf�rer");
		this.bNouveau = new JButton("Nouveau compte");
		this.bAllDetails = new JButton("D�tails des comptes");
		this.bDetails = new JButton("D�tails du compte");
		this.bExport = new JButton("Exporter mouvements");
		this.bSupprimer = new JButton("Fermer compte");
		this.bFermer = new JButton("Fermer");

		if (dlg.bAdministrateur) {
			pBouton.add(this.bDebiter);
			pBouton.add(this.bAllDetails);
			pBouton.add(this.bDetails);
			pBouton.add(this.bExport);
			pBouton.add(this.bModifierAgios);
			pBouton.add(this.bModifierInteret);
		}
		else {
			pBouton.add(this.bCrediter);
			pBouton.add(this.bDebiter);
			pBouton.add(this.bTransferer);
			pBouton.add(this.bNouveau);
			pBouton.add(this.bAllDetails);
			pBouton.add(this.bDetails);
			pBouton.add(this.bExport);
			pBouton.add(this.bSupprimer);
			pBouton.add(this.bFermer);
		}

		// Ajout de l'adaptateur qui g�re les events
		this.unAdaptateurBoutons = new AdaptateurBoutons();
		this.bCrediter.addActionListener(this.unAdaptateurBoutons);
		this.bDebiter.addActionListener(this.unAdaptateurBoutons);
		this.bNouveau.addActionListener(this.unAdaptateurBoutons);
		this.bAllDetails.addActionListener(this.unAdaptateurBoutons);
		this.bExport.addActionListener(this.unAdaptateurBoutons);
		this.bFermer.addActionListener(this.unAdaptateurBoutons);
		this.bSupprimer.addActionListener(this.unAdaptateurBoutons);
		this.bTransferer.addActionListener(this.unAdaptateurBoutons);
		this.bDetails.addActionListener(this.unAdaptateurBoutons);
		this.bModifierAgios.addActionListener(this.unAdaptateurBoutons);
		this.bModifierInteret.addActionListener(this.unAdaptateurBoutons);
		this.addWindowListener(new AdapFenetre());

		this.getContentPane().add("Center", this.listCompte);
		this.getContentPane().add("South", pBouton);
		this.setTitle("Liste des comptes");

		this.pack();
		this.show();

		this.getContentPane().add("Center", this.listCompte);
		this.getContentPane().add("South", pBouton);
		this.setTitle("Liste des comptes");

		WindowUtils.centerWindow(this);

		this.pack();
		this.show();
	}

	public void ReloadListe() {

		if (this.listCompte.getItemCount() > 0) {
			this.listCompte.removeAll();
		}

		if (this.iCodeClient == -1) {
			for (int i = 0; i < this.dlgMain.listeCompte.size(); i++) {
				this.listCompte.add(this.dlgMain.listeCompte.afficher(i));
			}
		}
		else {
			for (int i = 0; i < this.dlgMain.listeClient.size(); i++) {
				if (this.dlgMain.listeClient.getCode(i) == this.iCodeClient) {
					for (int j = 0; j < this.dlgMain.listeClient.nbCptForClient(this.iCodeClient); j++) {
						this.listCompte.add(Integer.toString(this.dlgMain.listeClient.cptClient(this.iCodeClient, j)));
					}
				}
			}
		}

		if (this.dlgMain.listeCompte.size() > 0) {
			this.listCompte.select(0);
		}

	}

	class AdaptateurBoutons implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {

			if (e.getSource() == DlgListeCompte.this.bAllDetails) {
				DlgListeCompte.this.theDlgDetailCompte = new DlgDetailCompte(DlgListeCompte.this.dlgMain);
			}
			else if (e.getSource() == DlgListeCompte.this.bDetails) {
				DlgListeCompte.this.theDlgDetailCompte = new DlgDetailCompte(DlgListeCompte.this.dlgMain,
						DlgListeCompte.this.listCompte.getSelectedIndex());
			}
			else if (e.getSource() == DlgListeCompte.this.bNouveau) {
				int iNextCode = DlgListeCompte.this.dlgMain.listeCompte.size();
				if (iNextCode != 0) {
					iNextCode = DlgListeCompte.this.dlgMain.listeCompte.getCodeCompte(DlgListeCompte.this.dlgMain.listeCompte.size() - 1) + 1;
				}
				DlgListeCompte.this.theDlgCreateCompte = new DlgCreateCompte(DlgListeCompte.this.dlgMain, -1, iNextCode);
				DlgListeCompte.this.theDlgCreateCompte.addWindowListener(new AdapFenetreCreate());
			}
			else if (e.getSource() == DlgListeCompte.this.bDebiter) {
				DlgListeCompte.this.theDlgDebiter = new DlgDebiter(DlgListeCompte.this.dlgMain, DlgListeCompte.this.listCompte.getSelectedIndex());
			}

			else if (e.getSource() == DlgListeCompte.this.bExport) {
				new DlgExportMouvements(DlgListeCompte.this.dlgMain, DlgListeCompte.this.listCompte.getSelectedIndex());
			}
			else if (e.getSource() == DlgListeCompte.this.bTransferer) {
				DlgListeCompte.this.theDlgTransferer = new DlgTransferer(DlgListeCompte.this.dlgMain, null);
			}
			else if (e.getSource() == DlgListeCompte.this.bFermer) {
				DlgListeCompte.this.setVisible(false);
			}
			else if (e.getSource() == DlgListeCompte.this.bCrediter) {
				DlgListeCompte.this.theDlgCrediter = new DlgCrediter(DlgListeCompte.this.dlgMain, DlgListeCompte.this.listCompte.getSelectedIndex());
			}
			else if (e.getSource() == DlgListeCompte.this.bSupprimer) {
				Comptes compteASupprimer = DlgListeCompte.this.dlgMain.listeCompte.getCompte(Integer.parseInt(DlgListeCompte.this.listCompte
						.getSelectedItem()));
				if (compteASupprimer.getSolde() != 0) {
					JOptionPane.showMessageDialog(null, "Impossible de ferme un compte avec un solde diff�rent de 0.", "Erreur : Fermeture annul�e",
							JOptionPane.ERROR_MESSAGE);
				}
				else {
					DlgListeCompte.this.dlgMain.listeCompte.supprimerComptes(Integer.parseInt(DlgListeCompte.this.listCompte.getSelectedItem()));
					DlgListeCompte.this.ReloadListe();
					DlgListeCompte.this.dlgMain.listeClient
							.removeCompteFromClient(Integer.parseInt(DlgListeCompte.this.listCompte.getSelectedItem()));
				}
			}
			else if (e.getSource() == DlgListeCompte.this.bModifierAgios) {

				Comptes compteToChange = DlgListeCompte.this.dlgMain.listeCompte.getCompte(Integer.parseInt(DlgListeCompte.this.listCompte
						.getSelectedItem()));
				if (compteToChange instanceof CompteDepot) {
					new DlgModifierAgios((CompteDepot) compteToChange);
				}
				else {
					JOptionPane.showMessageDialog(null, "Vous devez s�lectionner un compte d�pot");
				}
			}
			else if (e.getSource() == DlgListeCompte.this.bModifierInteret) {

				Comptes compteToChange = DlgListeCompte.this.dlgMain.listeCompte.getCompte(Integer.parseInt(DlgListeCompte.this.listCompte
						.getSelectedItem()));
				if (compteToChange instanceof CompteEpargne) {
					new DlgModifierInteret((CompteEpargne) compteToChange);
				}
				else {
					JOptionPane.showMessageDialog(null, "Vous devez s�lectionner un compte �pargne");
				}
			}
		}
	}// fin de AdaptateurBoutons

	class AdapFenetreCreate extends WindowAdapter {
		@Override
		public void windowDeactivated(WindowEvent e) {
			DlgListeCompte.this.ReloadListe();
		}
	}// fin de AdapFenetreCreate

	class AdapFenetre extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			DlgListeCompte.this.setVisible(false);
		}
	}

}
