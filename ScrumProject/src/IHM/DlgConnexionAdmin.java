//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\IHM\\DlgCrediter.java

package IHM;

import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

public class DlgConnexionAdmin extends JFrame {
	private String password_required = "admin";
	DlgMain dlgMain;

	JButton bAnnuler;
	JButton bSeConnecter;
	JPasswordField tfPassword;

	AdaptateurBoutons unAdaptateurBoutons;

	public DlgConnexionAdmin(DlgMain dlg) {
		this.dlgMain = dlg;
		Panel pPassword = new Panel();
		FlowLayout flPassword = new FlowLayout();
		pPassword.setLayout(flPassword);
		pPassword.add(new Label("Mot de passe :"));
		this.tfPassword = new JPasswordField("", 20);
		pPassword.add(this.tfPassword);

		Panel pBouton = new Panel();
		FlowLayout flBouton = new FlowLayout();
		pBouton.setLayout(flBouton);
		this.bSeConnecter = new JButton("Se connecter");
		this.bAnnuler = new JButton("Annuler");

		pBouton.add(this.bSeConnecter);
		pBouton.add(this.bAnnuler);

		this.getContentPane().add("North", pPassword);
		this.getContentPane().add("South", pBouton);

		this.setTitle("Connexion administrateur");

		this.unAdaptateurBoutons = new AdaptateurBoutons();
		this.bSeConnecter.addActionListener(this.unAdaptateurBoutons);
		this.bAnnuler.addActionListener(this.unAdaptateurBoutons);

		WindowUtils.centerWindow(this);

		this.pack();
		this.show();
	}

	class AdaptateurBoutons implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == DlgConnexionAdmin.this.bSeConnecter) {
				if (DlgConnexionAdmin.this.tfPassword.getText().equals(DlgConnexionAdmin.this.password_required)) {
					DlgConnexionAdmin.this.dlgMain.createWindow(true);
					DlgConnexionAdmin.this.setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "Le mot de passe renseign� n'est pas correct.", "Erreur : Connexion annul�e",
							JOptionPane.ERROR_MESSAGE);
				}
			}
			else if (e.getSource() == DlgConnexionAdmin.this.bAnnuler) {
				DlgConnexionAdmin.this.setVisible(false);
			}
		}
	}// fin de AdaptateurBoutons

	class AdapFenetre extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			DlgConnexionAdmin.this.setVisible(false);
		}
	}

}
