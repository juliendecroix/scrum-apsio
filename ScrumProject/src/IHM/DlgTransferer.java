//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\IHM\\DlgTransferer.java

package IHM;

import java.awt.Choice;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;

public class DlgTransferer extends JFrame {
	DlgMain dlgMain;

	Choice cbCompteDepart;
	Choice cbCompteArrivee;
	Choice cbClient;
	JButton bTransferer;
	JButton bAnnuler;
	TextField tfMontant;
	TextField tfDesc;
	int iCptSelDep, iCptSelArr;
	AdaptateurBoutons unAdaptateurBoutons;

	public DlgTransferer(DlgMain dlg, String sClient) {
		this.dlgMain = dlg;

		this.cbClient = new Choice();
		this.cbCompteDepart = new Choice();
		this.cbCompteArrivee = new Choice();
		this.iCptSelDep = 0;
		this.iCptSelArr = 0;

		for (int i = 0; i < this.dlgMain.listeClient.size(); i++) {
			this.cbClient.add(this.dlgMain.listeClient.afficher(i));
		}

		if (sClient != null) {
			this.cbClient.select(sClient);
		}

		this.ReloadListes();

		Panel pClient = new Panel();
		pClient.setLayout(new FlowLayout());
		pClient.add(new Label("Client :"));
		pClient.add(this.cbClient);

		Panel pCompteDepart = new Panel();
		pCompteDepart.setLayout(new FlowLayout());
		pCompteDepart.add(new Label("Compte source :"));
		pCompteDepart.add(this.cbCompteDepart);

		Panel pCompteArrivee = new Panel();
		pCompteArrivee.setLayout(new FlowLayout());
		pCompteArrivee.add(new Label("Compte destination :"));
		pCompteArrivee.add(this.cbCompteArrivee);

		Panel pMontant = new Panel();
		FlowLayout flMontant = new FlowLayout();
		pMontant.setLayout(flMontant);
		pMontant.add(new Label("Montant :"));
		this.tfMontant = new TextField("", 20);
		pMontant.add(this.tfMontant);

		Panel pBouton = new Panel();
		FlowLayout flBouton = new FlowLayout();
		pBouton.setLayout(flBouton);
		this.bTransferer = new JButton("Transf�rer");
		this.bAnnuler = new JButton("Annuler");
		pBouton.add(this.bTransferer);
		pBouton.add(this.bAnnuler);

		this.getContentPane().setLayout(new GridLayout(5, 1));
		this.getContentPane().add(pClient);
		this.getContentPane().add(pCompteDepart);
		this.getContentPane().add(pCompteArrivee);
		this.getContentPane().add(pMontant);
		this.getContentPane().add(pBouton);

		this.cbClient.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				DlgTransferer.this.ReloadListes();
			}
		}

		);

		this.cbCompteDepart.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				DlgTransferer.this.iCptSelDep = Integer.parseInt(DlgTransferer.this.cbCompteDepart.getSelectedItem());
			}
		}

		);

		this.cbCompteArrivee.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				DlgTransferer.this.iCptSelArr = Integer.parseInt(DlgTransferer.this.cbCompteArrivee.getSelectedItem());
			}
		}

		);

		this.unAdaptateurBoutons = new AdaptateurBoutons();
		this.bTransferer.addActionListener(this.unAdaptateurBoutons);
		this.bAnnuler.addActionListener(this.unAdaptateurBoutons);

		this.addWindowListener(new AdapFenetre());

		this.setTitle("Transf�rer compte � compte");

		WindowUtils.centerWindow(this);

		this.pack();
		this.show();
	}

	private void ReloadListes() {
		String sCodeCli = this.cbClient.getSelectedItem();
		int iEspace = sCodeCli.indexOf(" ");
		sCodeCli = sCodeCli.substring(0, iEspace);
		int iCode = Integer.parseInt(sCodeCli);

		if (this.cbCompteDepart.getItemCount() > 0) {
			this.cbCompteDepart.removeAll();
			this.cbCompteArrivee.removeAll();
		}

		for (int i = 0; i < this.dlgMain.listeCompte.size(); i++) {
			if (this.dlgMain.listeClient.appartientCompte(iCode, this.dlgMain.listeCompte.getCodeCompte(i))) {
				String sCode = this.dlgMain.listeCompte.afficher(i);
				this.cbCompteDepart.add(sCode);
				this.cbCompteArrivee.add(sCode);
			}
		}
	}

	class AdaptateurBoutons implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == DlgTransferer.this.bTransferer) {
				double dMont = Double.parseDouble(DlgTransferer.this.tfMontant.getText());
				int iCptDep = Integer.parseInt(DlgTransferer.this.cbCompteDepart.getSelectedItem());
				int iCptArr = Integer.parseInt(DlgTransferer.this.cbCompteArrivee.getSelectedItem());
				String sDescDep = "Transfert vers le compte " + iCptArr;
				String sDescArr = "Transfert du compte " + iCptDep;
				// D�bit du compte source
				boolean bOK = DlgTransferer.this.dlgMain.listeCompte.addMouvement(iCptDep, dMont * -1, sDescDep,
						DlgTransferer.this.dlgMain.bAdministrateur);
				if (bOK) {
					// Cr�dit du compte destination
					DlgTransferer.this.dlgMain.listeCompte.addMouvement(iCptArr, dMont, sDescArr, DlgTransferer.this.dlgMain.bAdministrateur);
				}

				DlgTransferer.this.setVisible(false);
			}
			else if (e.getSource() == DlgTransferer.this.bAnnuler) {
				DlgTransferer.this.setVisible(false);
			}
		}
	}// fin de AdaptateurBoutons

	class AdapFenetre extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			DlgTransferer.this.setVisible(false);
		}
	}

}
