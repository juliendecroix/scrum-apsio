//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\IHM\DlgCreateCompte.java

package IHM;

import java.awt.Choice;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

import Application.OuvrirCompte;

public class DlgCreateCompte extends JFrame {
	DlgMain dlgMain;

	public OuvrirCompte theOuvrirCompte;

	Choice cbClient;
	Choice cbTypeCompte;
	JButton bValider;
	JButton bAnnuler;
	TextField textfieldDepotInitial;
	TextField textfieldDecouvertAutorise;
	JRadioButton boutonCompteEpargne;
	JRadioButton boutonCompteDepot;
	int numeroCompte;
	AdaptateurBoutons unAdaptateurBoutons;

	public DlgCreateCompte(DlgMain dlg, int iCodeClient, int codeCompte) {
		this.dlgMain = dlg;

		Panel panelNumeroCompte = new Panel();
		FlowLayout layoutNumeroCompte = new FlowLayout();
		panelNumeroCompte.setLayout(layoutNumeroCompte);
		this.numeroCompte = codeCompte;
		Label labelStringNmCompte = new Label("Num�ro de compte :");
		Label labelNumeroCompte = new Label(Integer.toString(this.numeroCompte));
		panelNumeroCompte.add(labelStringNmCompte);
		panelNumeroCompte.add(labelNumeroCompte);

		this.cbClient = new Choice();
		this.ReloadClientListe();
		Panel pClient = new Panel();
		FlowLayout flClient = new FlowLayout();
		pClient.setLayout(flClient);
		Label lCli = new Label("Num�ro de client :");
		pClient.add(lCli);
		pClient.add(this.cbClient);

		Panel panelTypeCompte = new Panel();
		FlowLayout layoutTypeCompte = new FlowLayout();
		panelTypeCompte.setLayout(layoutTypeCompte);
		Label labelTypeCompte = new Label("Type de compte :");
		this.boutonCompteEpargne = new JRadioButton("Compte Epargne", true);
		this.boutonCompteDepot = new JRadioButton("Compte D�p�t");
		panelTypeCompte.add(labelTypeCompte);
		panelTypeCompte.add(this.boutonCompteEpargne);
		panelTypeCompte.add(this.boutonCompteDepot);

		ButtonGroup group = new ButtonGroup();
		group.add(this.boutonCompteEpargne);
		group.add(this.boutonCompteDepot);

		Panel panelDepotInitial = new Panel();
		FlowLayout layoutDepotInitial = new FlowLayout();
		panelDepotInitial.setLayout(layoutDepotInitial);
		Label lableDepotInitial = new Label("D�p�t initial :");
		this.textfieldDepotInitial = new TextField("", 20);
		panelDepotInitial.add(lableDepotInitial);
		panelDepotInitial.add(this.textfieldDepotInitial);

		Panel panelDecouvertAutorise = new Panel();
		FlowLayout layoutDecouvertAutorise = new FlowLayout();
		panelDecouvertAutorise.setLayout(layoutDecouvertAutorise);
		Label labelDecouvertAutorise = new Label("D�couvert autoris� :");
		this.textfieldDecouvertAutorise = new TextField(" ", 20);
		this.textfieldDecouvertAutorise.setEditable(false);
		panelDecouvertAutorise.add(labelDecouvertAutorise);
		panelDecouvertAutorise.add(this.textfieldDecouvertAutorise);

		Panel pBouton = new Panel();
		FlowLayout flBouton = new FlowLayout();
		pBouton.setLayout(flBouton);
		this.bValider = new JButton("Cr�er compte");
		this.bAnnuler = new JButton("Annuler");
		pBouton.add(this.bValider);
		pBouton.add(this.bAnnuler);

		this.getContentPane().setLayout(new GridLayout(6, 1));
		this.getContentPane().add(panelNumeroCompte);
		this.getContentPane().add(pClient);
		this.getContentPane().add(panelTypeCompte);
		this.getContentPane().add(panelDepotInitial);
		this.getContentPane().add(panelDecouvertAutorise);
		this.getContentPane().add(pBouton);

		// pr�selection du client
		if (iCodeClient != -1) {
			for (int i = 0; i < this.cbClient.getItemCount(); i++) {
				if (Integer.parseInt(this.cbClient.getItem(i)) == iCodeClient) {
					this.cbClient.select(i);
				}
			}
		}
		else { // selection par d�faut
			if (this.cbClient.getItemCount() > 0) {
				this.cbClient.select(0);
			}
			else // blocage de la fenetre
			{
				this.cbClient.setEnabled(false);
			}
		}

		this.unAdaptateurBoutons = new AdaptateurBoutons();
		this.bValider.addActionListener(this.unAdaptateurBoutons);
		this.bAnnuler.addActionListener(this.unAdaptateurBoutons);
		this.boutonCompteDepot.addActionListener(this.unAdaptateurBoutons);
		this.boutonCompteEpargne.addActionListener(this.unAdaptateurBoutons);
		this.addWindowListener(new AdapFenetre());

		this.setTitle("Cr�er un compte");

		WindowUtils.centerWindow(this);

		this.pack();
		this.show();
	}

	public void ReloadClientListe() {
		if (this.cbClient.getItemCount() > 0) {
			this.cbClient.removeAll();
		}

		for (int i = 0; i < this.dlgMain.listeClient.size(); i++) {
			String sCode = this.dlgMain.listeClient.afficher(i);
			int iEspace = sCode.indexOf(" ");
			sCode = sCode.substring(0, iEspace);
			this.cbClient.add(sCode);
		}
	}

	class AdaptateurBoutons implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == DlgCreateCompte.this.bValider) {
				double depotInitial = Double.parseDouble(DlgCreateCompte.this.textfieldDepotInitial.getText());
				Double decouvertAutorise;
				if (!DlgCreateCompte.this.textfieldDecouvertAutorise.getText().equals(" ")) {
					decouvertAutorise = Double.parseDouble(DlgCreateCompte.this.textfieldDecouvertAutorise.getText());
				}
				else {
					decouvertAutorise = null;
				}
				OuvrirCompte oc = new OuvrirCompte(DlgCreateCompte.this.dlgMain.listeClient, DlgCreateCompte.this.numeroCompte,
						Integer.parseInt(DlgCreateCompte.this.cbClient.getSelectedItem()), depotInitial, decouvertAutorise);
				if (DlgCreateCompte.this.boutonCompteDepot.isSelected()) {
					DlgCreateCompte.this.dlgMain.listeCompte.addCompteDepot(oc.createCompteDepot());
				}
				else {
					DlgCreateCompte.this.dlgMain.listeCompte.addCompteEpargne(oc.createCompteEpargne());
				}
				DlgCreateCompte.this.setVisible(false);
			}
			else if (e.getSource() == DlgCreateCompte.this.bAnnuler) {
				DlgCreateCompte.this.setVisible(false);
			}
			else if (e.getSource() == DlgCreateCompte.this.boutonCompteDepot) {
				DlgCreateCompte.this.textfieldDecouvertAutorise.setEditable(true);
			}
			else if (e.getSource() == DlgCreateCompte.this.boutonCompteEpargne) {
				DlgCreateCompte.this.textfieldDecouvertAutorise.setText(" ");
				DlgCreateCompte.this.textfieldDecouvertAutorise.setEditable(false);
			}
		}
	}// fin de AdaptateurBoutons

	class AdapFenetre extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			DlgCreateCompte.this.setVisible(false);
		}
	}

}
