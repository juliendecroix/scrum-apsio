//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\IHM\\DlgListeClient.java

package IHM;

import java.awt.FlowLayout;
import java.awt.List;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;

public class DlgListeClient extends JFrame {
	DlgMain dlgMain;
	public DlgCreateClient theDlgCreateClient;

	AdaptateurBoutons unAdaptateurBoutons;
	JButton bNouvCpt;
	JButton bNouveau;
	JButton bSupprimer;
	JButton bFermer;
	JButton bTransferer;
	JButton bListeCpt;
	List listClient;

	/**
	 * @roseuid 3D246158023A
	 */
	public DlgListeClient(DlgMain dlg) {
		this.dlgMain = dlg;

		this.listClient = new List(5, false);

		this.ReloadListe();

		Panel pBouton = new Panel();
		FlowLayout flBouton = new FlowLayout();
		pBouton.setLayout(flBouton);
		this.bNouveau = new JButton("Nouveau client");
		pBouton.add(this.bNouveau);
		this.bSupprimer = new JButton("Supprimer client");
		pBouton.add(this.bSupprimer);
		this.bNouvCpt = new JButton("Nouveau compte");
		pBouton.add(this.bNouvCpt);
		this.bListeCpt = new JButton("Lister comptes");
		pBouton.add(this.bListeCpt);
		this.bTransferer = new JButton("Transf�rer");
		pBouton.add(this.bTransferer);
		this.bFermer = new JButton("Fermer");
		pBouton.add(this.bFermer);

		this.unAdaptateurBoutons = new AdaptateurBoutons();
		this.bNouveau.addActionListener(this.unAdaptateurBoutons);
		this.bSupprimer.addActionListener(this.unAdaptateurBoutons);
		this.bFermer.addActionListener(this.unAdaptateurBoutons);
		this.bNouvCpt.addActionListener(this.unAdaptateurBoutons);
		this.bListeCpt.addActionListener(this.unAdaptateurBoutons);
		this.bTransferer.addActionListener(this.unAdaptateurBoutons);
		this.addWindowListener(new AdapFenetre());

		this.getContentPane().add("Center", this.listClient);
		this.getContentPane().add("South", pBouton);

		this.setTitle("Liste des clients");

		WindowUtils.centerWindow(this);

		this.pack();
		this.show();
	}

	public void ReloadListe() {

		if (this.listClient.getItemCount() > 0) {
			this.listClient.removeAll();
		}

		for (int i = 0; i < this.dlgMain.listeClient.size(); i++) {
			this.listClient.add(this.dlgMain.listeClient.afficher(i));
		}

		if (this.dlgMain.listeClient.size() > 0) {
			this.listClient.select(0);
		}

	}

	class AdaptateurBoutons implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {

			if (e.getSource() == DlgListeClient.this.bSupprimer) {
				String sCode = DlgListeClient.this.listClient.getSelectedItem();
				if (sCode != null) {
					int iEspace = sCode.indexOf(" ");
					sCode = sCode.substring(0, iEspace);
					int iCode = Integer.parseInt(sCode);

					for (int i = 0; i < DlgListeClient.this.dlgMain.listeClient.size(); i++) {
						int iCodeCli = DlgListeClient.this.dlgMain.listeClient.getCode(i);
						if (iCodeCli == iCode) {
							DlgListeClient.this.dlgMain.listeClient.supprimerClient(i);
							System.out.println("Client supprim� : " + sCode);
							DlgListeClient.this.ReloadListe();
						}
					}
				}
			}
			else if (e.getSource() == DlgListeClient.this.bNouveau) {
				int iNextCode = DlgListeClient.this.dlgMain.listeClient.size();
				if (iNextCode != 0) {
					iNextCode = DlgListeClient.this.dlgMain.listeClient.getCode(DlgListeClient.this.dlgMain.listeClient.size() - 1) + 1;
				}

				DlgListeClient.this.theDlgCreateClient = new DlgCreateClient(DlgListeClient.this.dlgMain, iNextCode);
				DlgListeClient.this.theDlgCreateClient.addWindowListener(new AdapFenetreCreate());
			}
			else if (e.getSource() == DlgListeClient.this.bFermer) {
				DlgListeClient.this.setVisible(false);
			}
			else if (e.getSource() == DlgListeClient.this.bListeCpt) {
				String sCode = DlgListeClient.this.listClient.getSelectedItem();
				int iEspace = sCode.indexOf(" ");
				sCode = sCode.substring(0, iEspace);
				int iCode = Integer.parseInt(sCode);

				new DlgListeCompte(DlgListeClient.this.dlgMain, iCode);
			}
			else if (e.getSource() == DlgListeClient.this.bNouvCpt) {
				int iNextCode = DlgListeClient.this.dlgMain.listeCompte.size();
				if (iNextCode != 0) {
					iNextCode = DlgListeClient.this.dlgMain.listeCompte.getCodeCompte(DlgListeClient.this.dlgMain.listeCompte.size() - 1) + 1;
				}

				String sCode = DlgListeClient.this.listClient.getSelectedItem();
				int iEspace = sCode.indexOf(" ");
				sCode = sCode.substring(0, iEspace);
				int iCode = Integer.parseInt(sCode);

				new DlgCreateCompte(DlgListeClient.this.dlgMain, iCode, iNextCode);
			}
			else if (e.getSource() == DlgListeClient.this.bTransferer) {
				new DlgTransferer(DlgListeClient.this.dlgMain, DlgListeClient.this.listClient.getSelectedItem());
			}
		}// fin de AdaptateurBoutons

		class AdapFenetreCreate extends WindowAdapter {
			@Override
			public void windowDeactivated(WindowEvent e) {
				DlgListeClient.this.ReloadListe();
			}
		}// fin de AdapFenetreCreate

	}// fin de AdaptateurBoutons

	class AdapFenetre extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			DlgListeClient.this.setVisible(false);
		}
	}// fin de AdapFenetre

}// fin de DlgListeClient

