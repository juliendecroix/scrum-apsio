package IHM;

import java.awt.Choice;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import Application.ExporterMouvements;
import Metier.Comptes;

public class DlgExportMouvements extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = -9044087909649087723L;

	Choice cbCompte;
	TextField path;
	Label label;
	JButton button;
	private Comptes compte;
	AdaptateurBoutons unAdaptateurBoutons;
	DlgMain main;

	DlgExportMouvements(DlgMain main, int iIndex) {
		this.main = main;
		this.unAdaptateurBoutons = new AdaptateurBoutons();

		// Récuperer le compte
		this.cbCompte = new Choice();
		this.ReloadListe();
		this.cbCompte.select(iIndex);
		int idCompte = Integer.parseInt(this.cbCompte.getItem(iIndex));

		this.compte = main.listeCompte.getCompte(idCompte);

		// create the elements
		this.label = new Label("Entrez le chemin de destination du fichier");
		this.path = new TextField("C:\\Users\\S219\\Documents\\ScrumExport.txt", 30);
		this.button = new JButton("Valider");

		// Add the elements
		Panel pLabel = new Panel();
		FlowLayout flCompte = new FlowLayout();
		pLabel.setLayout(flCompte);
		pLabel.add(this.label);

		Panel pPath = new Panel();
		FlowLayout flMontant = new FlowLayout();
		pPath.setLayout(flMontant);
		pPath.add(new Label("Path :"));
		pPath.add(this.path);

		Panel pBouton = new Panel();
		FlowLayout flBouton = new FlowLayout();
		pBouton.setLayout(flBouton);
		pBouton.add(this.button);

		// Add listener
		this.button.addActionListener(this.unAdaptateurBoutons);

		this.getContentPane().add("North", pLabel);
		this.getContentPane().add("Center", pPath);
		this.getContentPane().add("South", pBouton);

		this.setTitle("Exporter");

		WindowUtils.centerWindow(this);

		this.pack();
		this.show();

	}

	public void ReloadListe() {
		if (this.cbCompte.getItemCount() > 0) {
			this.cbCompte.removeAll();
		}

		for (int i = 0; i < this.main.listeCompte.size(); i++) {
			String sCode = this.main.listeCompte.afficher(i);
			this.cbCompte.add(sCode);
		}
	}

	class AdaptateurBoutons implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == DlgExportMouvements.this.button) {
				new ExporterMouvements(DlgExportMouvements.this.compte, DlgExportMouvements.this.path.getText());
				DlgExportMouvements.this.setVisible(false);
			}
		}
	}
}
