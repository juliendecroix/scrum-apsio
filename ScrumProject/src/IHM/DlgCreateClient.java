//Source file: c:\\Mes documents\\Lecomte - Barbieri\\Projet UML-Java\\IHM\\DlgCreateClient.java

package IHM;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;

import Application.CreerClient;

public class DlgCreateClient extends JFrame {
	DlgMain dlgMain;
	public CreerClient theCreerClient;

	JButton bValider;
	JButton bAnnuler;
	JButton bCompte;
	Label lCode;
	TextField tfNom;
	TextField tfAdr;
	AdaptateurBoutons unAdaptateurBoutons;

	/**
	 * @roseuid 3D2461550226
	 */
	public DlgCreateClient(DlgMain dlg, int iCodeClient) {
		this.dlgMain = dlg;

		Panel pCode = new Panel();
		FlowLayout flCode = new FlowLayout();
		pCode.setLayout(flCode);
		pCode.add(new Label("Code :"));
		this.lCode = new Label(Integer.toString(iCodeClient));
		pCode.add(this.lCode);

		Panel pNom = new Panel();
		FlowLayout flNom = new FlowLayout();
		pNom.setLayout(flNom);
		pNom.add(new Label("Nom :"));
		this.tfNom = new TextField("", 20);
		pNom.add(this.tfNom);

		Panel pAdresse = new Panel();
		FlowLayout flAdresse = new FlowLayout();
		pAdresse.setLayout(flAdresse);
		pAdresse.add(new Label("Adresse :"));
		this.tfAdr = new TextField("", 20);
		pAdresse.add(this.tfAdr);

		Panel pClient = new Panel();
		BorderLayout blClient = new BorderLayout();
		pClient.setLayout(blClient);
		pClient.add("North", pCode);
		pClient.add("Center", pNom);
		pClient.add("South", pAdresse);

		Panel pBouton = new Panel();
		FlowLayout flBouton = new FlowLayout();
		pBouton.setLayout(flBouton);
		this.bValider = new JButton("Cr�er client");
		this.bCompte = new JButton("Compte");
		this.bAnnuler = new JButton("Annuler");
		this.bCompte.setEnabled(false);

		pBouton.add(this.bValider);
		pBouton.add(this.bCompte);
		pBouton.add(this.bAnnuler);

		this.getContentPane().add("North", pClient);
		this.getContentPane().add("South", pBouton);

		this.unAdaptateurBoutons = new AdaptateurBoutons();
		this.bAnnuler.addActionListener(this.unAdaptateurBoutons);
		this.bValider.addActionListener(this.unAdaptateurBoutons);
		this.bCompte.addActionListener(this.unAdaptateurBoutons);
		this.addWindowListener(new AdapFenetre());

		this.setTitle("Nouveau client");

		WindowUtils.centerWindow(this);

		this.pack();
		this.show();
	}

	class AdaptateurBoutons implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == DlgCreateClient.this.bAnnuler) {
				DlgCreateClient.this.setVisible(false);
			}
			else if (e.getSource() == DlgCreateClient.this.bValider) {
				if (DlgCreateClient.this.bValider.getText().equals("Cr�er client")) {
					String sNom, sAdr, sCode;
					sNom = DlgCreateClient.this.tfNom.getText();
					sAdr = DlgCreateClient.this.tfAdr.getText();
					sCode = DlgCreateClient.this.lCode.getText();

					if (!sNom.equals("") && !sAdr.equals("")) {
						DlgCreateClient.this.dlgMain.listeClient.add(sNom, sAdr, Integer.parseInt(sCode));
						DlgCreateClient.this.bAnnuler.setText("Fermer");
						DlgCreateClient.this.bValider.setText("Nouveau");
						DlgCreateClient.this.bCompte.setEnabled(true);
						DlgCreateClient.this.tfAdr.setEnabled(false);
						DlgCreateClient.this.tfNom.setEnabled(false);
					}
					else {
						System.out.println("Les champs ne doivent pas etre vide");
					}
				}
				else if (DlgCreateClient.this.bValider.getText().equals("Nouveau")) {
					DlgCreateClient.this.bValider.setText("Cr�er client");
					DlgCreateClient.this.bCompte.setEnabled(false);
					DlgCreateClient.this.tfAdr.setEnabled(true);
					DlgCreateClient.this.tfNom.setEnabled(true);
					DlgCreateClient.this.tfAdr.setText("");
					DlgCreateClient.this.tfNom.setText("");
					DlgCreateClient.this.lCode.setText(Integer.toString(Integer.parseInt(DlgCreateClient.this.lCode.getText()) + 1));
				}
			}
			else if (e.getSource() == DlgCreateClient.this.bCompte) {
				int iNextCode = DlgCreateClient.this.dlgMain.listeCompte.size();
				if (iNextCode != 0) {
					iNextCode = DlgCreateClient.this.dlgMain.listeCompte.getCodeCompte(DlgCreateClient.this.dlgMain.listeCompte.size() - 1) + 1;
				}
				new DlgCreateCompte(DlgCreateClient.this.dlgMain, Integer.parseInt(DlgCreateClient.this.lCode.getText()), iNextCode);

			}
		}
	}// fin de AdaptateurBoutons

	class AdapFenetre extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			DlgCreateClient.this.setVisible(false);
		}
	}// fin de AdapFenetre

}// fin de DlgCreateClient
