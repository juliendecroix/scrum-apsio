package IHM;

import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import Metier.CompteDepot;

public class DlgModifierAgios extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = 3188693373243048415L;
	TextField tfMontant;
	JButton bModifier;
	AdaptateurBoutons adapteur;
	CompteDepot compte;

	public DlgModifierAgios(CompteDepot compteToChange) {
		Panel panel = new Panel();
		this.compte = compteToChange;
		this.adapteur = new AdaptateurBoutons();

		panel.add(new Label("Entrez la nouvelle valeur du pourcentage de l'agios (1 = 100%)"));

		this.tfMontant = new TextField("", 5);
		this.bModifier = new JButton("Valider");

		panel.add(this.tfMontant);
		panel.add(this.bModifier);

		this.add(panel);

		this.bModifier.addActionListener(this.adapteur);

		this.setTitle("Modifier Agios du compte " + compteToChange.getCodeCompte());

		WindowUtils.centerWindow(this);

		this.pack();
		this.show();
	}

	class AdaptateurBoutons implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == DlgModifierAgios.this.bModifier) {
				DlgModifierAgios.this.compte.setAgios(Double.parseDouble(DlgModifierAgios.this.tfMontant.getText()));
				DlgModifierAgios.this.setVisible(false);
			}
		}
	}// fin de AdaptateurBoutons
}
