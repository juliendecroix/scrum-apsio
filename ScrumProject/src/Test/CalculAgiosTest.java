package Test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import Application.ListeClient;
import Application.OuvrirCompte;
import Metier.CompteDepot;

public class CalculAgiosTest {
	@SuppressWarnings("deprecation")
	@Test
	public void testCalculAgios() throws Exception {
		
		/***POSITIF***/
		//On ouvre un compte de depot avec un solde positif
		OuvrirCompte ouvrirComptePositif = new OuvrirCompte(new ListeClient(), 8000, 1, 100, 0.0);
		CompteDepot comptePositif = ouvrirComptePositif.createCompteDepot();
		//On v�rifie que les agios sont bien �gal � 0�
		assertTrue(comptePositif.calculerAgios() == 0);
		System.out.print("Agios CoptePositif: " + comptePositif.calculerAgios() + "\n");
		
		/***NEGATIF***/
		//On ouvre un compte de depot avec un solde positif
		OuvrirCompte ouvrirCompteNegatif = new OuvrirCompte(new ListeClient(), 9000, 2, 0, 500.0);
		CompteDepot compteNegatif = ouvrirCompteNegatif.createCompteDepot();
		//On debite assez le compte pour que le solde passe en negatif
		compteNegatif.debiter(300, "Achat meuble", false);
		//On v�rifie que les agios = 60� (0.2*solde*(-1))===(0.2*(-300)*(-1)) 
		Assert.assertEquals(compteNegatif.calculerAgios(), 60.0, 1e-15);
		System.out.print("Agios CompteNegatif: " + compteNegatif.calculerAgios() + "\n");
	}
}
