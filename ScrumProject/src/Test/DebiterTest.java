package Test;

import org.junit.Assert;
import org.junit.Test;

import Application.ListeClient;
import Application.ListeCompte;
import Application.OuvrirCompte;
import Metier.CompteDepot;

public class DebiterTest {
	@Test
	public void testDebitExceptionel() throws Exception {
		// Situation initial : Un compte depot avec un solde valant 50 et un
		// decouvert de 0
		OuvrirCompte ouvrirCompte = new OuvrirCompte(new ListeClient(), 8000, 2, 50, 0.0);
		CompteDepot compte = ouvrirCompte.createCompteDepot();

		ListeCompte listeCompte = new ListeCompte();
		listeCompte.addCompteDepot(compte);

		// Quand : L'utilisateur en mode non-admin essaye de retirer 60
		boolean debiter = compte.debiter(60, "erreur", false);

		// Alors : On re�oit false et le solde est de 50

		Assert.assertFalse(debiter);
		Assert.assertTrue(compte.getSolde() == 50);

		// Quand : On passe en mode admin et on essaye de retirer 60
		debiter = compte.debiter(60, "good", true);

		// Alors : On re�oit true et le solde est � -10
		Assert.assertTrue(debiter);
		Assert.assertTrue(compte.getSolde() == -10);
	}
}
