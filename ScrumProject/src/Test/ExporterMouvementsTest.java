package Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import Application.ExporterMouvements;
import Application.ListeClient;
import Application.ListeCompte;
import Application.OuvrirCompte;
import Metier.CompteDepot;

public class ExporterMouvementsTest {

	@Test
	public void testExportMouvement() throws Exception {
		// Situation Initial : On a un compte avec de l'argent et un certain
		// nombre de mouvement
		OuvrirCompte ouvrirCompte = new OuvrirCompte(new ListeClient(), 8000, 2, 50, 200d);
		CompteDepot compte = ouvrirCompte.createCompteDepot();

		ListeCompte listeCompte = new ListeCompte();
		listeCompte.addCompteDepot(compte);

		compte.crediter(100d, "credit");
		compte.debiter(100d, "retrait", true);
		// Quand : J'exporte pour ces mouvements
		new ExporterMouvements(compte, "C:\\Users\\S219\\Documents\\ScrumExport.txt");

		// Alors : Chaque ligne devrait etre celle attendue
		FileReader fr = new FileReader(new File("C:\\Users\\S219\\Documents\\ScrumExport.txt"));
		BufferedReader reader = null;

		reader = new BufferedReader(fr);

		String currentLine;
		ArrayList<String> lines = new ArrayList<String>();
		while ((currentLine = reader.readLine()) != null) {
			lines.add(currentLine);
		}

		String string1 = "0 50.0 " + compte.theMouvements.get(0).getDate() + " D�p�t initial";
		String string2 = "1 100.0 " + compte.theMouvements.get(1).getDate() + " credit";
		String string3 = "2 -100.0 " + compte.theMouvements.get(2).getDate() + " retrait";
		Assert.assertTrue(lines.get(0).equals(string1));
		Assert.assertTrue(lines.get(1).equals(string2));
		Assert.assertTrue(lines.get(2).equals(string3));
		Assert.assertTrue(lines.size() == 3);
		reader.close();

	}
}
