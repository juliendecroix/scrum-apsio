package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import Application.ListeClient;
import Application.OuvrirCompte;
import IHM.DlgMain;
import Metier.CompteDepot;

public class CompteDepotTest {

	
	@Test
	public void testDecouvertAutoriseParDefaut() throws Exception {
		// Quand : Je cr�e un compte depot
		OuvrirCompte ouvrirCompte = new OuvrirCompte(new ListeClient(), 8000, 1, 0, null);
		CompteDepot compte = ouvrirCompte.createCompteDepot();
		// Alors : Le compte cr�� doit avoir un d�couvert autoris� de 0
		assertTrue(compte.getDecouvertAutorise() == 0);
	}
	
	@Test
	public void testDecouvertAutoriseFixed() throws Exception {
		// Quand : Je cr�e un compte avec un d�couvert autoris� fix� � une valeur positive
		OuvrirCompte ouvrirCompte = new OuvrirCompte(new ListeClient(), 8000, 1, 0, 50.0);
		CompteDepot compte = ouvrirCompte.createCompteDepot();
		// Alors : Le compte cr�� doit avoir un d�couvert autoris� valant ce que l'on a indiqu�
		assertTrue(compte.getDecouvertAutorise() == 50);
	}
	
	@Test
	public void testDecouvertNegatif() throws Exception {
		// Quand : Je cr�e un compte avec un d�couvert autoris� fix� � une valeur n�gative
		OuvrirCompte ouvrirCompte = new OuvrirCompte(new ListeClient(), 8000, 1, 0, -50.0);
		CompteDepot compte = ouvrirCompte.createCompteDepot();
		// Alors : Le compte cr�� doit avoir un d�couvert autoris� valant la valeur par d�faut
		assertTrue(compte.getDecouvertAutorise() == 0);
	}
}
